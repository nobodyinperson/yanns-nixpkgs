{ pkgs ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/7f256d7da238cb627ef189d56ed590739f42f13b.tar.gz")
  { } }:
pkgs.git-annex.overrideAttrs (oldAttrs: rec {
  version = "${src.rev}"; # Set the custom version
  src = pkgs.fetchgit {
    url = "git://git-annex.branchable.com/";
    rev = "588cda3833f749de2af3b0086d05edcb6666a24c";
    sha256 = "sha256-5XRivRoniHQdX7iLL3SVsffPxLKZmQTA9nmEbRKqKjI=";
  };
})
