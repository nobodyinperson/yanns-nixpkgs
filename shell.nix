{ pkgs ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/7f256d7da238cb627ef189d56ed590739f42f13b.tar.gz")
  { } }:
let
  git-annex = import ./git-annex.nix { };
  camp2ascii = import ./camp2ascii.nix { };
in pkgs.mkShell { buildInputs = [ camp2ascii git-annex ]; }
