{ pkgs ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/7f256d7da238cb627ef189d56ed590739f42f13b.tar.gz")
  { } }:

with pkgs;
stdenv.mkDerivation rec {
  pname = "camp2ascii";
  version = "${src.rev}";

  src = fetchFromGitHub {
    owner = "nobodyinperson";
    repo = "camp2ascii";
    rev = "0c677a7f5f583026e086334c1718dec1cf883554";
    sha256 = "sha256-v+bS1fjWoZD3aDu1bl+WQEa9oJH++NSTKHk7Mq7Y2Es=";
  };

  buildFlags = [ "EXTRA_CFLAGS=-Wno-format-security" ];
  installPhase = "install -Dm755 bin/camp2ascii -t $out/bin";

  meta = with lib; {
    description =
      "Conversion of Campbell Scientific TOB1, TOB2 and TOB3 data files to ASCII formats";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
}
